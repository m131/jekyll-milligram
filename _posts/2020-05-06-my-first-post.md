---
layout: post
title: Mi primer post
author: Mario Abreu
categories: [post, prueba, jekyll, bulma]
image: https://picsum.photos/seed/milli/376/150
---

Lorem ipsum **dolor sit amet**, consectetur adipisicing elit. Repellendus quisquam explicabo quibusdam commodi odit soluta, provident, molestias adipisci. Eum itaque labore quae aspernatur *velit nesciunt* a veritatis ipsa, culpa, incidunt!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, accusantium.

[Link](https://github.com)

![Picsum](https://picsum.photos/seed/picsum/300)

# Jekyll Milligram
This is a very minimalistic theme for [Jekyll](https://jekyllrb.com) made it with [Milligram](https://milligram.io) framework.

**Features**:
- Minimalistic
- Responsive
- Header image for posts
- About page
- Only two social icons



![Theme Screenshot](jm_theme.png)

![Post screenshot](jm_post.png)